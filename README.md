# 사원관리 API

***

#### LANGUAGE
```
JAVA 14
SpringBoot 2.7.3
```
#### 기능
```
* 공지사항
  - 공지사항 내용 삭제 (Del)
  - 공지사항 모두 가저오기 (Get)
  - 공지사항 상세 가저오기 (Get)
  - 공지사항 내용 등록 (Post)
  - 공지사항 내용 수정 (Put)

* 근태관리(관리자)
  - 금일 출퇴근 현황 (Get)
 
* 근태관리(사용자)
  - 근무일수 확인 (Get)
  - 출퇴근 날짜 확인 (Get)
  - 근태 상태 변경 (Put)
  - 근태 상태 가저오기 (Get)
 
* 로그인 관리
  - 관리자 로그인 (Post)
  - 사용자 로그인 (Post)
  - 비밀번호 변경 (Put)
  
* 배너 관리
  - 배너 하나 가저오기 (Get)
  - 배너 모두 가저오기 (Get)
  - 배너 수정 (Put)
  - 배너 삭제 (Del)
  - 배너 등록 (Post)
  
* 사원정보(사용자)
  - 사원 탈퇴 (Del)
  - MyPage 가저오기 (Get)
  - 회원 폰번호 수정 (Put)
  
* 사원 정보 관리(관리자)
  - 사원 퇴사 (Del)
  - 사원정보 모두 가저오기 (Get)
  - 부서로 검색하기 (Get)
  - 사원한명 저보 가저오기 (Get)
  - 관리자 권한 부여 (Put)
  - 사원 정보 수정 (Put)
  - 사원 이름을 검색 (Get)
  - 사원 정보 등록 (Post)
  - 사원 직급으로 검색 (Get)
  
* 휴가 정보(관리자)
  - 휴가승인 (Put)
  - 휴가신청서 가저오기 (Get)
  - 연차 추가 (Put)
  
* 휴가 신청(사용자)
  - 휴가 신청 현황 (Get)
  - 휴가 사용 현황 (Get)
  - 휴가 신청 (Post)
  

```


 * Swagger 전체
![SwaggerList](./images/swaggerList.png)


 * Swagger 공지사항
![Notice](./images/notice.png)


* Swagger 근태관리(관리자)
![AttendanceManager](./images/attendanceManager.png)


* Swagger 근태관리(사용자)
![AttendanceUse](./images/attendanceUse.png)


* Swagger 로그인
![Login](./images/login.png)


* Swagger 배너
![Banner](./images/banner.png)


* Swagger 사원정보(사용자)
![MemberUse](./images/memberUse.png)


* Swagger 사원정보(관리자)
![MemberManager](./images/memberManager.png)


* Swagger 휴가정보(관리자)
![HolidayManager](./images/holidayManager.png)


* Swagger 휴가신청(사용자)
![HolidayUse](./images/holidayUse.png)
