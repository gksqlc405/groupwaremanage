package com.chb.groupwareManage.advice;

import com.chb.groupwareManage.enums.ResultCode;
import com.chb.groupwareManage.exception.*;
import com.chb.groupwareManage.model.CommonResult;
import com.chb.groupwareManage.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);

    }

    @ExceptionHandler(CNoEmployeeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoEmployeeException e) {
        return ResponseService.getFailResult(ResultCode.NO_EMPLOYEE);

    }

    @ExceptionHandler(CNoAttendanceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoAttendanceException e) {
        return ResponseService.getFailResult(ResultCode.NO_ATTENDANCE);
    }

    @ExceptionHandler(CNoLeaveWorkException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoLeaveWorkException e) {
        return ResponseService.getFailResult(ResultCode.NO_LEAVE_WORK);
    }

    @ExceptionHandler(CNoUserNameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoUserNameException e) {
        return ResponseService.getFailResult(ResultCode.NO_USER_NAME);
    }

    @ExceptionHandler(CNoPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoPasswordException e) {
        return ResponseService.getFailResult(ResultCode.NO_PASSWORD);
    }

    @ExceptionHandler(CNoIsEnabledException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoIsEnabledException e) {
        return ResponseService.getFailResult(ResultCode.NO_IS_ENABLED);
    }

    @ExceptionHandler(CNoCheckPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoCheckPasswordException e) {
        return ResponseService.getFailResult(ResultCode.NO_CHECK_PASSWORD);
    }

    @ExceptionHandler(CAlreadyAttendanceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAlreadyAttendanceException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_ATTENDANCE);
    }

    @ExceptionHandler(CNoStatusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoStatusException e) {
        return ResponseService.getFailResult(ResultCode.NO_STATUS);
    }

    @ExceptionHandler(CNoSameStatusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoSameStatusException e) {
        return ResponseService.getFailResult(ResultCode.NO_SAME_STATUS);
    }


    @ExceptionHandler(CSameUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CSameUsernameException e) {
        return ResponseService.getFailResult(ResultCode.SAME_USERNAME);
    }


    @ExceptionHandler(CApprovedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CApprovedException e) {
        return ResponseService.getFailResult(ResultCode.APPROVED);
    }


    @ExceptionHandler(CCompanionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CCompanionException e) {
        return ResponseService.getFailResult(ResultCode.COMPANION);
    }

    @ExceptionHandler(CNoRemainingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoRemainingException e) {
        return ResponseService.getFailResult(ResultCode.REMAINING);
    }

    @ExceptionHandler(CNoHolidayRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoHolidayRequestException e) {
        return ResponseService.getFailResult(ResultCode.NO_HOLIDAY_REQUEST);
    }
}
