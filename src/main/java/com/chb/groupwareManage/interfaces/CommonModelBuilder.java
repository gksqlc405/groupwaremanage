package com.chb.groupwareManage.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
