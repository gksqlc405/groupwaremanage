package com.chb.groupwareManage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroupwareManageApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroupwareManageApplication.class, args);
	}

}
