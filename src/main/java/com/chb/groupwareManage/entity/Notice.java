package com.chb.groupwareManage.entity;

import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import com.chb.groupwareManage.model.manager.NoticeRequest;
import com.chb.groupwareManage.model.manager.NoticeUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.yaml.snakeyaml.events.Event;

import javax.persistence.*;
import javax.swing.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Notice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 150)
    private String note;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putNotice(NoticeUpdateRequest request) {
        this.note = request.getNote();
        this.dateUpdate = LocalDateTime.now();
    }

    private Notice(NoticeBuilder builder) {
        this.note = builder.note;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;

    }

    public static class NoticeBuilder implements CommonModelBuilder<Notice> {
        private final String note;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public NoticeBuilder(NoticeRequest request) {
            this.note = request.getNote();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Notice build() {
            return new Notice(this);
        }
    }
}

