package com.chb.groupwareManage.entity;

import com.chb.groupwareManage.enums.AttendanceStatus;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceManage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "사원", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberManageId", nullable = false)
    private MemberManage memberManage;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 11)
    private AttendanceStatus attendanceType;

    @Column(nullable = false)
    private LocalDate dateBase;

    @Column(nullable = false)
    private LocalTime dateAttendance;


    private LocalTime dateLeaveWork;


    private LocalTime dateLeaveEarly;

    public void putStatus(AttendanceStatus attendanceType) {
        this.attendanceType = attendanceType;

        switch (attendanceType) {
            case LEAVE_EARLY:
                this.dateLeaveEarly = LocalTime.now();
                break;
            case LEAVE_WORK:
                this.dateLeaveWork = LocalTime.now();
                break;
        }
    }


    private AttendanceManage(AttendanceManageBuilder builder) {
        this.memberManage = builder.memberManage;
        this.attendanceType = builder.attendanceType;
        this.dateBase = builder.dateBase;
        this.dateAttendance = builder.dateAttendance;

    }


    public static class AttendanceManageBuilder implements CommonModelBuilder<AttendanceManage> {
        private final MemberManage memberManage;
        private final AttendanceStatus attendanceType;
        private final LocalDate dateBase;
        private final LocalTime dateAttendance;

        public AttendanceManageBuilder(MemberManage memberManage) {
            this.memberManage = memberManage;
            this.attendanceType = AttendanceStatus.ATTENDANCE;
            this.dateBase = LocalDate.now();
            this.dateAttendance = LocalTime.now();
        }




        @Override
        public AttendanceManage build() {
            return new AttendanceManage(this);
        }
    }


    private AttendanceManage(AttendanceManageNoneValueBuilder builder) {
        this.attendanceType = builder.attendanceType;
    }

    public static class AttendanceManageNoneValueBuilder implements CommonModelBuilder<AttendanceManage> {
        private final AttendanceStatus attendanceType;

        public AttendanceManageNoneValueBuilder() {
            this.attendanceType = AttendanceStatus.NONE;

        }

        @Override
        public AttendanceManage build() {
            return new AttendanceManage(this);
        }
    }


}
