package com.chb.groupwareManage.entity;

import com.chb.groupwareManage.enums.AttendanceStatus;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Attendance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //출근회원
    @Column(nullable = false)
    private Long memberId;

    // 출근일
    @Column(nullable = false)
    private LocalDate dateWork;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private AttendanceStatus attendanceStatus;

    //출근시간
    @Column(nullable = false)
    private LocalTime timeStart;

    //조퇴시간
    private LocalTime timeLeaveEarly;  // 출근을 누르기전엔 필요가없기때문에 필수가아니다

    //퇴근시간
    private LocalTime timeLeave;  // 출근을 누르기전엔 필요가없기때문에 필수가아니다


    public void putStatus(AttendanceStatus attendanceStatus) {
        this.attendanceStatus = attendanceStatus;

        switch (attendanceStatus) {
            case LEAVE_EARLY:  // 조퇴일때 조퇴시간이 찍히게하고
                this.timeLeaveEarly = LocalTime.now();
                break;
            case LEAVE_WORK:  // 퇴근일때 퇴근 시간이 찍히게 한다.
                this.timeLeave = LocalTime.now();
        }
    }

    private Attendance(AttendanceTestBuilder builder) {
        this.memberId = builder.memberId;
        this.dateWork = builder.dateWork;
        this.attendanceStatus = builder.attendanceStatus;
        this.timeStart = builder.timeStart;

    }

    public  static class AttendanceTestBuilder implements CommonModelBuilder<Attendance> {

        //위에 nullable false 로 필수로 줘야한다 했기때문에 값을 채워준다
        private final Long memberId;
        private final LocalDate dateWork;
        private final AttendanceStatus attendanceStatus;
        private final LocalTime timeStart;

        public AttendanceTestBuilder(Long memberId) {   //memberId 만 받아온 이유는 나머지값들은 앱에서 조작이 가능하기 때문에 값을 넣을필요가 없다

            this.memberId = memberId;
            this.dateWork = LocalDate.now();
            this.attendanceStatus = AttendanceStatus.ATTENDANCE;
            this.timeStart = LocalTime.now();
        }


        @Override
        public Attendance build() {
            return new Attendance(this);
        }
    }

}
