package com.chb.groupwareManage.entity;

import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import com.chb.groupwareManage.model.manager.BannerUrlRequest;
import com.chb.groupwareManage.model.manager.BannerUrlUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BannerUrl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 150)
    private String url;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime datUpdate;

    public void putBanner(BannerUrlUpdateRequest request) {
        this.url = request.getUrl();
        this.datUpdate = LocalDateTime.now();
    }

    private BannerUrl(BannerUrlBuilder builder) {
        this.url = builder.url;
        this.dateCreate = builder.dateCreate;
        this.datUpdate = builder.datUpdate;

    }

    public static class BannerUrlBuilder implements CommonModelBuilder<BannerUrl> {
        private final String url;
        private final LocalDateTime dateCreate;
        private final LocalDateTime datUpdate;

        public BannerUrlBuilder(BannerUrlRequest request) {
            this.url = request.getUrl();
            this.dateCreate = LocalDateTime.now();
            this.datUpdate = LocalDateTime.now();
        }

        @Override
        public BannerUrl build() {
            return new BannerUrl(this);
        }
    }
}
