package com.chb.groupwareManage.entity;


import com.chb.groupwareManage.enums.ApprovalStatus;
import com.chb.groupwareManage.enums.HolidayType;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import com.chb.groupwareManage.model.user.HolidayRegistrationRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidayHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "회원", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private MemberManage memberManage;

    @ApiModelProperty(notes = "차감여부(true 면 차감, false 면 증가)")
    //@Column(nullable = false)
    private Boolean isMinus;

    @ApiModelProperty(notes = "증감값")
    //@Column(nullable = false)
    private Float increaseOrDecreaseValue;

    @ApiModelProperty(notes = "요청 시간")
    @Column(nullable = false)
    private LocalDate dateCreate;

    @ApiModelProperty(notes = "승인 상태")
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private ApprovalStatus approvalStatus;

    @ApiModelProperty(notes = "휴가 사유")
    @Column(length = 100)
    private String reason;

    @ApiModelProperty(notes = "관리자 승인여부, true 면 승인, false 면 반려 ")
    @Column(nullable = false)
    private Boolean isManager;

    @ApiModelProperty(notes = "휴무시작일")
    private LocalDate dateHolidayStart;

    @ApiModelProperty(notes = "휴무 끝날")
    private LocalDate dateHolidayEnd;

    @ApiModelProperty(notes = "승인시간")
    private LocalDateTime dateApproval;

    @ApiModelProperty(notes = "반려시간")
    private LocalDateTime dateCompanion;

    @ApiModelProperty(notes = "휴가 종류")
    @Column(length = 20)
    @Enumerated(value = EnumType.STRING)
    private HolidayType holidayType;



    public void putApprovalStatus() {
        this.approvalStatus = ApprovalStatus.APPROVED;
    }

    public void putCompanions() {
        this.approvalStatus = ApprovalStatus.COMPANION;
    }



    public void putApprovedTime() {
        this.approvalStatus = ApprovalStatus.APPROVED;
        this.dateCreate = LocalDate.now();
        this.dateApproval = LocalDateTime.now();
        this.isManager = true;
    }

    public void putCompanionTime() {
        this.approvalStatus = ApprovalStatus.COMPANION;
        this.dateCreate = LocalDate.now();
        this.dateCompanion = LocalDateTime.now();
        this.isManager = false;
    }


    private HolidayHistory(HolidayHistoryBuilder builder) {
        this.memberManage = builder.memberManage;
        this.dateCreate = builder.dateCreate;
        this.approvalStatus = builder.approvalStatus;
        this.reason = builder.reason;
        this.isManager = builder.isManager;
        this.dateHolidayStart = builder.dateHolidayStart;
        this.dateHolidayEnd = builder.dateHolidayEnd;
        this.holidayType = builder.holidayType;
        this.increaseOrDecreaseValue = builder.increaseOrDecreaseValue;
        this.isMinus = builder.isMinus;

    }


    public static class HolidayHistoryBuilder implements CommonModelBuilder<HolidayHistory> {
        private final MemberManage memberManage;

        private final LocalDate dateCreate;
        private final ApprovalStatus approvalStatus;
        private final String reason;
        private final Boolean isManager;
        private final LocalDate dateHolidayStart;
        private final LocalDate dateHolidayEnd;
        private final HolidayType holidayType;
        private final Float  increaseOrDecreaseValue;
        private final Boolean isMinus;


        public HolidayHistoryBuilder(
                MemberManage memberManage, HolidayRegistrationRequest request) {
            this.memberManage = memberManage;
            this.dateCreate = LocalDate.now();
            this.approvalStatus = ApprovalStatus.REQUESTING;
            this.reason = request.getReason();
            this.isManager = false;
            this.dateHolidayStart = request.getDateHolidayStart();
            this.dateHolidayEnd = request.getDateHolidayEnd();
            this.holidayType = request.getHolidayType();
            this.increaseOrDecreaseValue = request.getIncreaseOrDecreaseValue();
            this.isMinus = true;


        }

        @Override
        public HolidayHistory build() {
            return new HolidayHistory(this);
        }
    }
}

