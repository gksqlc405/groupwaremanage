package com.chb.groupwareManage.entity;

import com.chb.groupwareManage.enums.Department;
import com.chb.groupwareManage.enums.Position;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import com.chb.groupwareManage.model.login.PasswordUpdateRequest;
import com.chb.groupwareManage.model.manager.IsAdminUpdateRequest;
import com.chb.groupwareManage.model.manager.MemberJoinRequest;
import com.chb.groupwareManage.model.manager.MemberManagerUpdateRequest;
import com.chb.groupwareManage.model.user.MyPagePhoneUpdate;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberManage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 아이디
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    // 비밀번호
    @Column(nullable = false, length = 20)
    private String password;

    // 사원명
    @Column(nullable = false, length = 20)
    private String name;

    // 사원 전화번호
    @Column(nullable = false, length = 20)
    private String phone;

    // 부서
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 15)
    private Department department;

    // 직급
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 23)
    private Position position;

    // 사원 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 사원 생년월일
    @Column(nullable = false)
    private LocalDate birthday;

    // 입사일
    @Column(nullable = false)
    private LocalDate dateIn;

    // 퇴사일
    private LocalDate dateOut;

    // 퇴사 여부
    @Column(nullable = false)
    private Boolean isEnabled;

    // 관리자 여부
    @Column(nullable = false)
    private Boolean isAdmin;


    public void putPassWord(PasswordUpdateRequest request) {
        this.password = request.getCheckPassword();
    }


    public void putPhoneNumber(MyPagePhoneUpdate myPagePhoneUpdate) {
        this.phone = myPagePhoneUpdate.getPhone();
    }


    public void putDateOut() {
        this.isEnabled = false;
        this.dateOut = LocalDate.now();
    }

    public void putIsAdmin(IsAdminUpdateRequest request) {
        this.isAdmin = request.getIsAdmin();
    }

    public void putMemberInfo(MemberManagerUpdateRequest request) {
        this.name = request.getName();
        this.phone = request.getPhone();
        this.department =request.getDepartment();
        this.position = request.getPosition();
        this.address = request.getAddress();
    }

    private MemberManage(MemberManageBuilder builder) {
             this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.phone = builder.phone;
        this.department = builder.department;
        this.position = builder.position;
        this.address = builder.address;
        this.birthday = builder.birthday;
        this.dateIn = builder.dateIn;
        this.isEnabled = builder.isEnabled;
        this.isAdmin = builder.isAdmin;


    }


    public static class MemberManageBuilder implements CommonModelBuilder<MemberManage> {

        private final String username;
        private final String password;
        private final String name;
        private final String phone;
        private final Department department;
        private final Position position;
        private final String address;
        private final LocalDate birthday;
        private final LocalDate dateIn;
        private final Boolean isEnabled;
        private final Boolean isAdmin;

        public MemberManageBuilder(MemberJoinRequest joinRequest) {
            this.username = joinRequest.getUsername();
            this.password = joinRequest.getPassword();
            this.name = joinRequest.getName();
            this.phone = joinRequest.getPhone();
            this.department = joinRequest.getDepartment();
            this.position = joinRequest.getPosition();
            this.address = joinRequest.getAddress();
            this.birthday = joinRequest.getBirthday();
            this.dateIn = joinRequest.getDateIn();
            this.isEnabled = true;
            this.isAdmin = false;

        }


        @Override
        public MemberManage build() {
            return new MemberManage(this);
        }
    }





}
