package com.chb.groupwareManage.entity;

import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidayCount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long memberId;
    @ApiModelProperty(notes = "총 연차 갯수")
    @Column(nullable = false)
    private Float countTotal;

    @ApiModelProperty(notes = "사용 연차 갯수")
    @Column(nullable = false)
    private Float countUse;

    @ApiModelProperty(notes = "연차 갯수 유효 시작일")
    @Column(nullable = false)
    private LocalDate dateHolidayCountStart;

    @ApiModelProperty(notes = "연차 갯수 유효 종료일")
    @Column(nullable = false)
    private LocalDate dateHolidayCountEnd;

    @ApiModelProperty(notes = "등록시간")
    @Column(nullable = false)
    private LocalDate dateCreate;

    @ApiModelProperty(notes = "수정시간")
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    @ApiModelProperty(notes = "잔여 연차 갯수")
    @Column(nullable = false)
    private Float countRemaining;

    public void plusCountTotal(float plusTotalCount) {
        this.countTotal += plusTotalCount;
        this.countRemaining += plusTotalCount;
        this.dateUpdate = LocalDateTime.now();
    }

    public void plusCountUse(float plusUseCount) {
        this.countUse += plusUseCount;
        this.countRemaining -= plusUseCount;
    }




    private HolidayCount(HolidayCountBuilder builder) {
        this.memberId = builder.memberId;
        this.countTotal = builder.countTotal;
        this.countUse = builder.countUse;
        this.dateHolidayCountStart = builder.dateHolidayCountStart;
        this.dateHolidayCountEnd = builder.dateHolidayCountEnd;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
        this.countRemaining = builder.countRemaining;

    }


    public static class HolidayCountBuilder implements CommonModelBuilder<HolidayCount> {
        private final Long memberId;
        private final Float countTotal;
        private final Float countUse;
        private final LocalDate dateHolidayCountStart;
        private final LocalDate dateHolidayCountEnd;
        private final LocalDate dateCreate;
        private final LocalDateTime dateUpdate;
        private final Float countRemaining;

        public HolidayCountBuilder(Long memberId, LocalDate dateIn) {
            this.memberId = memberId;
            this.countTotal = 0f;
            this.countUse = 0f;
            this.dateHolidayCountStart = dateIn;
            this.dateHolidayCountEnd = dateHolidayCountStart.plusYears(1).minusDays(1);
            this.dateCreate = LocalDate.now();
            this.dateUpdate = LocalDateTime.now();
            this.countRemaining = 0f;
        }

        @Override
        public HolidayCount build() {
            return new HolidayCount(this);
        }
    }
}

