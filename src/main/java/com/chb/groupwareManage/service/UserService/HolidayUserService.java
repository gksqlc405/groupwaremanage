package com.chb.groupwareManage.service.UserService;

import com.chb.groupwareManage.entity.HolidayCount;
import com.chb.groupwareManage.entity.HolidayHistory;
import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.exception.CMissingDataException;
import com.chb.groupwareManage.exception.CNoHolidayRequestException;
import com.chb.groupwareManage.exception.CNoRemainingException;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.user.HolidayRegistrationRequest;
import com.chb.groupwareManage.model.user.HolidayStatusItem;
import com.chb.groupwareManage.model.user.HolidayUseItem;
import com.chb.groupwareManage.repository.HolidayCountRepository;
import com.chb.groupwareManage.repository.HolidayHistoryRepository;
import com.chb.groupwareManage.repository.MemberRepository;
import com.chb.groupwareManage.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class HolidayUserService {

    private final HolidayHistoryRepository holidayHistoryRepository;
    private final HolidayCountRepository holidayCountRepository;

    private final MemberRepository memberRepository;


    /**
     * 사원 연차 신청
     *
     * @param memberManage  사원 entity
     * @param request  연차 신청할 정보들
     */
    public void setHolidayApply(MemberManage memberManage, HolidayRegistrationRequest request) {

        HolidayHistory holidayHistory = new HolidayHistory.HolidayHistoryBuilder(memberManage, request).build();

        HolidayCount holidayCount = holidayCountRepository.findById(memberManage.getId()).orElseThrow(CMissingDataException::new);


        if (holidayCount.getCountRemaining() < holidayHistory.getIncreaseOrDecreaseValue())
            throw new CNoRemainingException();

        holidayHistoryRepository.save(holidayHistory);

    }


    /**
     * 휴가 사용 현황
     *
     * @param memberId  사원 시퀀스
     * @param dateYear  휴가사용한 년도
     * @return  휴가 사용 정보
     */
    public HolidayUseItem getHolidayUse(long memberId, int dateYear ) {

        LocalDate dateStart = LocalDate.of(
                dateYear, 1, 1
        );

        LocalDate dateEnd = LocalDate.of(
                dateYear, 12, 31
        );



        HolidayCount holidayCount = holidayCountRepository.findByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndMemberId(dateStart, dateEnd, memberId);
        return new HolidayUseItem.HolidayUseItemBuilder(holidayCount).build();
    }


    /**
     * 휴가 신청 현황
     *
     * @param memberManage  사원 entity
     * @param dateYear 휴가 신청한 년도
     * @return 휴가 신청 리스트
     */
    public ListResult<HolidayStatusItem> getHolidayStatus(MemberManage memberManage, int dateYear) {
        List<HolidayStatusItem> result = new LinkedList<>();

        LocalDate dateStart = LocalDate.of(
                dateYear, 1, 1
        );

        LocalDate dateEnd = LocalDate.of(
                dateYear, 12, 31
        );

        List<HolidayHistory> holidayHistories = holidayHistoryRepository.findAllByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndMemberManage_Id(dateStart, dateEnd,memberManage.getId());

        if (holidayHistories.isEmpty()) throw new CNoHolidayRequestException();

        holidayHistories.forEach(holidayHistory -> {
            HolidayStatusItem addItem = new HolidayStatusItem.HolidayStatusItemBuilder(holidayHistory).build();
            result.add(addItem);

        });

        return ListConvertService.settingResult(result);

    }



}

