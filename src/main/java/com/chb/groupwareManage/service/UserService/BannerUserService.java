package com.chb.groupwareManage.service.UserService;

import com.chb.groupwareManage.entity.BannerUrl;
import com.chb.groupwareManage.exception.CMissingDataException;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.user.BannerUrlItem;
import com.chb.groupwareManage.repository.BannerUrlRepository;
import com.chb.groupwareManage.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BannerUserService {
    private final BannerUrlRepository bannerUrlRepository;


    /**
     * 배너 하나 가저오기
     *
     * @param id  배너 아이디
     * @return  가저올 배너 정보들
     */
    public BannerUrlItem getBanner(long id) {
        BannerUrl bannerUrl = bannerUrlRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new BannerUrlItem.BannerUrlItemBuilder(bannerUrl).build();
    }


    /**
     * 배너 리스트 가저오기
     *
     * @return  가저올 배너 리스트
     */
    public ListResult<BannerUrlItem> getBanners() {
        List<BannerUrlItem> result = new LinkedList<>();

        List<BannerUrl> bannerUrls = bannerUrlRepository.findAll();
        bannerUrls.forEach(bannerUrl -> {
            BannerUrlItem addItem = new BannerUrlItem.BannerUrlItemBuilder(bannerUrl).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }
}
