package com.chb.groupwareManage.service.UserService;

import com.chb.groupwareManage.entity.Notice;
import com.chb.groupwareManage.exception.CMissingDataException;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.user.NoticeItem;
import com.chb.groupwareManage.repository.NoticeRepository;
import com.chb.groupwareManage.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NoticeUserService {
    private final NoticeRepository noticeRepository;


    /**
     * 공지사항 하나 가저오기
     *
     * @param id  공지사항 리스트
     * @return  공지사항 정보
     */
    public NoticeItem getNotice(long id) {
        Notice notice = noticeRepository.findById(id).orElseThrow(CMissingDataException::new);
       return new NoticeItem.NoticeItemBuilder(notice).build();

    }


    /**
     * 공지사항 리스트 가저오기
     *
     * @return 공지사항 리스트
     */
    public ListResult<NoticeItem> getNotices() {
        List<NoticeItem> result = new LinkedList<>();

        List<Notice> notices = noticeRepository.findAll();
        notices.forEach(notice -> {
            NoticeItem addItem = new NoticeItem.NoticeItemBuilder(notice).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

}
