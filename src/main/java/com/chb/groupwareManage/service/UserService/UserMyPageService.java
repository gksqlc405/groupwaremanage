package com.chb.groupwareManage.service.UserService;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.enums.Department;
import com.chb.groupwareManage.enums.Position;
import com.chb.groupwareManage.exception.CMissingDataException;
import com.chb.groupwareManage.exception.CNoCheckPasswordException;
import com.chb.groupwareManage.exception.CNoEmployeeException;
import com.chb.groupwareManage.exception.CNoPasswordException;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.manager.MemberItem;
import com.chb.groupwareManage.model.user.MyPagePhoneUpdate;
import com.chb.groupwareManage.model.user.UserMyPageItem;
import com.chb.groupwareManage.model.user.UserWithdrawalRequest;
import com.chb.groupwareManage.repository.MemberRepository;
import com.chb.groupwareManage.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserMyPageService {
    private final MemberRepository memberRepository;

    /**
     * myPage 가저오기
     *
     * @param memberId myPage 가저올 사원 시퀀스
     * @return  마이페이지 정보
     */
    public UserMyPageItem getMyPage(long memberId) {
        MemberManage memberManage = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        return new UserMyPageItem.UserMyPageItemBuilder(memberManage).build();
    }

    /**
     * 탈퇴하기
     *
     * @param memberId  탈퇴할 사원 시퀀스
     * @param request  탈퇴에 필요한 비밀번호
     */
    public void putWithdrawal(long memberId, UserWithdrawalRequest request) {
        MemberManage memberManage = memberRepository.findByIdAndPassword(memberId,request.getPassword()).orElseThrow(CNoPasswordException::new);

        if (!memberManage.getPassword().equals(request.getPassword())) throw new CNoPasswordException();

        if (!request.getPassword().equals(request.getCheckPassword())) throw new CNoCheckPasswordException();

        if (!memberManage.getIsEnabled()) throw new CNoEmployeeException();

        memberManage.putDateOut();
        memberRepository.save(memberManage);
    }

    /**
     * myPage 사원 전화번호 수정
     *
     * @param memberId 전화번호 수정할 사원 시퀀스
     * @param myPagePhoneUpdate  수정할 전화번호
     */
    public void putPhoneNumber(long memberId, MyPagePhoneUpdate myPagePhoneUpdate) {
        MemberManage memberManage = memberRepository.findById(memberId).orElseThrow();
        memberManage.putPhoneNumber(myPagePhoneUpdate);
        memberRepository.save(memberManage);
    }


}
