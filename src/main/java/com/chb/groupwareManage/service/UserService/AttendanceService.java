package com.chb.groupwareManage.service.UserService;

import com.chb.groupwareManage.entity.Attendance;
import com.chb.groupwareManage.enums.AttendanceStatus;
import com.chb.groupwareManage.exception.CAlreadyAttendanceException;
import com.chb.groupwareManage.exception.CNoSameStatusException;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.user.AttendanceCount;
import com.chb.groupwareManage.model.user.AttendanceResponse;
import com.chb.groupwareManage.model.user.AttendanceTestModel;
import com.chb.groupwareManage.repository.AttendanceRepository;
import com.chb.groupwareManage.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttendanceService {
    private final AttendanceRepository attendanceRepository;

    /**
     * 근태 상태 가저오기
     *
     * @param memberId  사원 시퀀스
     * @return  근태 상태 정보
     */
    public AttendanceResponse getCurrentStatus(long memberId) {
        Optional<Attendance> attendance = attendanceRepository.findByDateWorkAndMemberId(LocalDate.now(), memberId);

        if (attendance.isEmpty()) return new  AttendanceResponse.AttendanceResponseNoneBuilder().build();
        else return new AttendanceResponse.AttendanceResponseBuilder(attendance.get()).build();
    }


    /**
     * 근태 상태 변경
     *
     * @param memberId  변경할 사원 시퀀스
     * @param attendanceStatus  근태 상태
     * @return  근태 상태 변경 정보
     */
    public AttendanceResponse doAttendanceChange(long memberId, AttendanceStatus attendanceStatus) { // 어떤회원이 어떤 상태로 바꿀려고한다
        Optional<Attendance> attendance = attendanceRepository.findByDateWorkAndMemberId(LocalDate.now(), memberId); // 오늘의 멤버 상태

        Attendance attendanceResult;
        if (attendance.isEmpty()) attendanceResult = setAttendance(memberId);
        else attendanceResult = putAttendance(attendance.get(), attendanceStatus);

        return new AttendanceResponse.AttendanceResponseBuilder(attendanceResult).build();
    }


    /**
     * 근태 상태 등록
     *
     * @param memberId  사원 시퀀스
     * @return 근태 엔티티
     */
    private Attendance setAttendance(long memberId) {
        Attendance data = new Attendance.AttendanceTestBuilder(memberId).build();
        return attendanceRepository.save(data);
    }


    /**
     *  근태상태 변경할 소스
     *
     * @param attendance 근태 entity
     * @param attendanceStatus  근태 상태 enum
     * @return  근태 entity
     */
    private Attendance putAttendance(Attendance attendance, AttendanceStatus attendanceStatus) {

        // 출근 후에는 다시 출근상태로 변경 할 수 없다.
        if (attendanceStatus.equals(AttendanceStatus.ATTENDANCE)) throw new CAlreadyAttendanceException();
        // 같은 상태로는 변경할수없다.
        if (attendance.getAttendanceStatus().equals(attendanceStatus)) throw new CNoSameStatusException();
        // 퇴근 후에는 상태를 변경 할 수 없다.
        if (attendance.getAttendanceStatus().equals(AttendanceStatus.LEAVE_WORK)) throw new CNoSameStatusException();

        attendance.putStatus(attendanceStatus);
        return attendanceRepository.save(attendance);

    }


    /**
     * 출퇴근 날짜 확인
     *
     * @param dateStart  확인할 시작날짜
     * @param dateEnd  확인할 끝 날짜
     * @param memberId  사원 시퀀스
     * @return  출퇴근 날짜 정보들
     */
    public ListResult<AttendanceTestModel> getAttendance(LocalDate dateStart, LocalDate dateEnd, long memberId ) {
        LocalDate dateStartTime = LocalDate.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth()
        );
        LocalDate dateEndTime = LocalDate.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth()
        );


        List<Attendance> attendanceTests =
                attendanceRepository.findAllByDateWorkGreaterThanEqualAndDateWorkLessThanEqualAndMemberIdOrderByDateWorkAsc(dateStartTime, dateEndTime, memberId);
        List<AttendanceTestModel> result = new LinkedList<>();
        attendanceTests.forEach(attendanceTest -> {
            AttendanceTestModel addItem = new AttendanceTestModel.AttendanceTestModelBuilder(attendanceTest).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);

    }


    /**
     * 특정회원의 특정 년/월 근무 일수 구하기
     *
     * @param memberId  사원 시퀀스
     * @param year  특정 년
     * @param month 특정 월
     * @return  근무일수 정보
     */
    public AttendanceCount getCountByMyYearMonth(long memberId, int year, int month ) {


        LocalDate dateStart = LocalDate.of(year, month ,1);

        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1 ,1 );
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        LocalDate dateEnd = LocalDate.of(year, month, maxDay);

        long countResult = attendanceRepository.countByMemberIdAndDateWorkGreaterThanEqualAndDateWorkLessThanEqual(memberId, dateStart, dateEnd);
        return new AttendanceCount.AttendanceLongBuilder(countResult).build();

    }

}

