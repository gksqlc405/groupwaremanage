package com.chb.groupwareManage.service;

import com.chb.groupwareManage.enums.ResultCode;
import com.chb.groupwareManage.model.CommonResult;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.SingleResult;
import org.springframework.stereotype.Service;

@Service
public class ResponseService {
    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess) {
        if (isSuccess) setSuccessResult(result);
        else setFailResult(result);
        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setDate(data);
        setSuccessResult(result);
        return result;
    }

    public static CommonResult getSuccessResult() {
        CommonResult result = new CommonResult();
        setSuccessResult(result);
        return result;
    }

    public static CommonResult getFailResult(ResultCode resultCode) {
        CommonResult result = new CommonResult();
        result.setIsSuccess(false);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMsg());
        return result;
    }

    private static void setSuccessResult(CommonResult result) {
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
    }

    private static void setFailResult(CommonResult result) {
        result.setIsSuccess(false);
        result.setCode(ResultCode.FAILED.getCode());
        result.setMsg(ResultCode.FAILED.getMsg());
    }


    public static <T> SingleResult<T> getLoginResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setDate(data);
        setLoginResult(result);
        return result;
    }

    private static void setLoginResult(CommonResult result) {
        result.setIsSuccess(true);
        result.setCode(ResultCode.LOGIN.getCode());
        result.setMsg(ResultCode.LOGIN.getMsg());
    }

    public static CommonResult getPasswordSuccessResult() {
        CommonResult result = new CommonResult();
        setPasswordSuccessResult(result);
        return result;
    }

    private static void setPasswordSuccessResult(CommonResult result) {
        result.setIsSuccess(true);
        result.setCode(ResultCode.PASSWORD.getCode());
        result.setMsg(ResultCode.PASSWORD.getMsg());
    }

    public static CommonResult getWithdrawal() {
        CommonResult result = new CommonResult();
        setWithdrawal(result);
        return result;
    }

    private static void setWithdrawal(CommonResult result) {
        result.setIsSuccess(true);
        result.setCode(ResultCode.WITHDRAWAL.getCode());
        result.setMsg(ResultCode.WITHDRAWAL.getMsg());
    }

    public static CommonResult getAttendance() {
        CommonResult result = new CommonResult();
        setAttendance(result);
        return result;
    }

    private static void setAttendance(CommonResult result) {
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS_ATTENDANCE.getCode());
        result.setMsg(ResultCode.SUCCESS_ATTENDANCE.getMsg());
    }

}
