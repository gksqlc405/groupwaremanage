package com.chb.groupwareManage.service.ManagerService;

import com.chb.groupwareManage.entity.AttendanceManage;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.manager.AttendanceManagerItem;
import com.chb.groupwareManage.repository.AtRepository;
import com.chb.groupwareManage.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AttendanceManagerService {
    private final AtRepository attendanceRepository;


    /**
     * 금일 출퇴근 현황
     *
     * @return 출퇴근 현황 정보
     */
    public ListResult<AttendanceManagerItem> getAttendances() {
        List<AttendanceManagerItem> result = new LinkedList<>();

        List<AttendanceManage> attendanceManages = attendanceRepository.findAllByDateBase(LocalDate.now());
        attendanceManages.forEach(attendanceManage -> {
            AttendanceManagerItem addItem = new AttendanceManagerItem.AttendanceManagerItemBuilder(attendanceManage).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

}
