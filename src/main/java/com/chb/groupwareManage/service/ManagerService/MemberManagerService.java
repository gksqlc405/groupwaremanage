package com.chb.groupwareManage.service.ManagerService;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.enums.Department;
import com.chb.groupwareManage.enums.Position;
import com.chb.groupwareManage.exception.CMissingDataException;
import com.chb.groupwareManage.exception.CNoEmployeeException;
import com.chb.groupwareManage.exception.CNoIsEnabledException;
import com.chb.groupwareManage.exception.CSameUsernameException;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.manager.IsAdminUpdateRequest;
import com.chb.groupwareManage.model.manager.MemberItem;
import com.chb.groupwareManage.model.manager.MemberJoinRequest;
import com.chb.groupwareManage.model.manager.MemberManagerUpdateRequest;
import com.chb.groupwareManage.repository.MemberRepository;
import com.chb.groupwareManage.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MemberManagerService {
    private final MemberRepository memberRepository;


    /**
     * 사원 시퀀스 가저오기
     *
     * @param memberId 사원 시퀀스
     * @return  사원 엔티티
     */
    public MemberManage getMemberId(long memberId) {
       return memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
    }


    /**
     * 사원 등록
     *
     * @param joinRequest  사원등록 정보들
     * @return 사원 엔티티
     */
    public MemberManage setMember(MemberJoinRequest joinRequest) {
        Optional<MemberManage> checkMember = memberRepository.findByUsername(joinRequest.getUsername());

        if (checkMember.isPresent()) throw new CSameUsernameException();

        MemberManage memberManage = new MemberManage.MemberManageBuilder(joinRequest).build();

        return memberRepository.save(memberManage);
    }


    /**
     * 사원한명 정보 가저오기
     *
     * @param memberId  사원 시퀀스
     * @return  사원 정보
     */
    public MemberItem getMember(long memberId) {
        MemberManage memberManage = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        return new MemberItem.MemberItemBuilder(memberManage).build();
    }


    /**
     * 사원 정보 모두 가저오기
     *
     * @param isEnable  true면 다니고있고 false면 퇴사
     * @return 사원 정보들
     */
    public ListResult<MemberItem> getMembers(boolean isEnable) {
        List<MemberItem> result = new LinkedList<>();

        List<MemberManage> memberManages = memberRepository.findAllByIsEnabledOrderByIdDesc(isEnable);
        memberManages.forEach(memberManage -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(memberManage).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }


    /**
     * 사원 직급 가저오기
     *
     * @param position  직급
     * @return  사원 정보들
     */
    public ListResult<MemberItem> getPosition(Position position) {
        List<MemberItem> result = new LinkedList<>();

        List<MemberManage> memberManages = memberRepository.findAllByPositionAndIsEnabledTrueOrderByIdDesc(position);

        if (memberManages.isEmpty()) throw new CNoEmployeeException();

        memberManages.forEach(memberManage -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(memberManage).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }


    /**
     * 사원 부서 가저오기
     *
     * @param department  부서
     * @return  사원 정보들
     */
    public ListResult<MemberItem> getDepartment(Department department) {
        List<MemberItem> result = new LinkedList<>();

        List<MemberManage> memberManages = memberRepository.findAllByDepartmentAndIsEnabledTrueOrderByIdDesc(department);

        if (memberManages.isEmpty()) throw new CNoEmployeeException();

        memberManages.forEach(memberManage -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(memberManage).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }


    /**
     * 사원 이름으로 검색해서 가저오기
     *
     * @param name  사원 이름
     * @return 사원 정보
     */
    public ListResult<MemberItem> getName (String name) {
        List<MemberItem> result = new LinkedList<>();

        List<MemberManage> memberManages = memberRepository.findAllByNameOrderByIdDesc(name);
        if (memberManages.isEmpty()) throw new CNoEmployeeException();

        memberManages.forEach(memberManage -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(memberManage).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }


    /**
     * 사원 정보 수정
     *
     * @param memberId  사원 시퀀스
     * @param request  사원 수정할 정보들
     */
    public void putMemberInfo(long memberId, MemberManagerUpdateRequest request) {
        MemberManage memberManage = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        memberManage.putMemberInfo(request);
        memberRepository.save(memberManage);
    }


    /**
     * 관리자 권한 부여
     *
     * @param memberId  사원 시퀀스
     * @param request  true 면 관리자 false면 사원
     */
    public void putIsAdmin(long memberId, IsAdminUpdateRequest request) {
        MemberManage memberManage = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        memberManage.putIsAdmin(request);
        memberRepository.save(memberManage);
    }


    /**
     * 사원 탈퇴 시키기
     *
     * @param memberId  사원 시퀀스
     * 사원 탈퇴지만 정보가 삭제되지 않고 남겨놔야하기 때문에 Delete 가 아닌 put 으로 한다
     */
    public void putDateOut(long memberId) {
        MemberManage memberManage = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);

        if (!memberManage.getIsEnabled()) throw new CNoEmployeeException();

        memberManage.putDateOut();
        memberRepository.save(memberManage);
    }



}
