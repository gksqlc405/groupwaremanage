package com.chb.groupwareManage.service.ManagerService;

import com.chb.groupwareManage.entity.BannerUrl;
import com.chb.groupwareManage.exception.CMissingDataException;
import com.chb.groupwareManage.model.manager.BannerUrlRequest;
import com.chb.groupwareManage.model.manager.BannerUrlUpdateRequest;
import com.chb.groupwareManage.repository.BannerUrlRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BannerManagerService {
    private final BannerUrlRepository bannerUrlRepository;

    /**
     * 배너 등록
     *
     * @param request 배너에 들어갈 url
     */

    public void setBanner(BannerUrlRequest request) {
        BannerUrl bannerUrl = new BannerUrl.BannerUrlBuilder(request).build();
        bannerUrlRepository.save(bannerUrl);
    }

    /**
     * 배너 수정
     *
     * @param id 배너 시퀀스
     * @param request 수정할 정보들
     */
    public void putBanner(long id, BannerUrlUpdateRequest request) {
        BannerUrl bannerUrl = bannerUrlRepository.findById(id).orElseThrow(CMissingDataException::new);
        bannerUrl.putBanner(request);
        bannerUrlRepository.save(bannerUrl);
    }

    /**
     *  배너 삭제
     *
     * @param id  삭제할 배너 시퀀스
     */
    public void delBanner(long id) {
        bannerUrlRepository.deleteById(id);
    }
}
