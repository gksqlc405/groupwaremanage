package com.chb.groupwareManage.service.ManagerService;

import com.chb.groupwareManage.entity.Notice;
import com.chb.groupwareManage.exception.CMissingDataException;
import com.chb.groupwareManage.model.manager.NoticeRequest;
import com.chb.groupwareManage.model.manager.NoticeUpdateRequest;
import com.chb.groupwareManage.repository.NoticeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NoticeManagerService {
    private final NoticeRepository noticeRepository;

    /**
     * 공지사항 등록
     *
     * @param request 공지사항 등록할 정보들
     */
    public void setNotice(NoticeRequest request) {
        Notice notice = new Notice.NoticeBuilder(request).build();
        noticeRepository.save(notice);
    }


    /**
     * 공지사항 수정
     *
     * @param id  공지사항 시퀀스
     * @param request  공지사항 수정할 정보들
     */
    public void putNotice(long id, NoticeUpdateRequest request) {
        Notice notice = noticeRepository.findById(id).orElseThrow(CMissingDataException::new);
        notice.putNotice(request);
        noticeRepository.save(notice);
    }


    /**
     * 공지사항 삭제
     *
     * @param id 공지사항 시퀀스
     */
    public void delNotice(long id) {
        noticeRepository.deleteById(id);
    }
}
