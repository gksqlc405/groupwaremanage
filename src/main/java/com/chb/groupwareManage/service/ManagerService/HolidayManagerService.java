package com.chb.groupwareManage.service.ManagerService;

import com.chb.groupwareManage.entity.HolidayCount;
import com.chb.groupwareManage.entity.HolidayHistory;
import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.enums.ApprovalStatus;
import com.chb.groupwareManage.exception.*;
import com.chb.groupwareManage.model.manager.HolidayItem;
import com.chb.groupwareManage.repository.HolidayCountRepository;
import com.chb.groupwareManage.repository.HolidayHistoryRepository;
import com.chb.groupwareManage.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.lang.reflect.Member;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HolidayManagerService {
    private final HolidayCountRepository holidayCountRepository;
    private final HolidayHistoryRepository holidayHistoryRepository;
    private final MemberRepository memberRepository;


    public HolidayHistory getHistoryId(long id) {
        return holidayHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
    }


    /**
     * 입사후 휴가 첫 등록
     *
     * @param memberManage 사원 시퀀스
     * @param dateIn  사원 입사일
     */
    public void setHoliday(MemberManage memberManage, LocalDate dateIn) {
        HolidayCount holidayCount = new HolidayCount.HolidayCountBuilder(memberManage.getId(), dateIn).build();
        holidayCountRepository.save(holidayCount);
    }


    /**
     * 휴가 승인
     *
     * @param holidayHistory  홀리데이 히스토리 시퀀스
     * @param isManger 승인여부
     */

    public void putHolidayCount(HolidayHistory holidayHistory, boolean isManger) {


        HolidayHistory holidayHistoryId = holidayHistoryRepository.findById(holidayHistory.getId()).orElseThrow(CMissingDataException::new);


        if (holidayHistory.getApprovalStatus().equals(ApprovalStatus.APPROVED)) throw  new CApprovedException();
        if (holidayHistory.getApprovalStatus().equals(ApprovalStatus.COMPANION)) throw new CCompanionException();


        if (isManger) holidayHistory.putApprovedTime();
        else holidayHistory.putCompanionTime();

        holidayHistoryRepository.save(holidayHistoryId);


        HolidayCount holidayCount = holidayCountRepository.findById(holidayHistory.getMemberManage().getId()).orElseThrow(CMissingDataException::new);
        if (isManger) holidayCount.plusCountUse(holidayHistory.getIncreaseOrDecreaseValue());

        if (isManger) holidayHistory.putApprovalStatus();
        else holidayHistory.putCompanions();


        holidayHistoryRepository.save(holidayHistory);



    }

    /**
     * 휴가 추가하기
     *
     * @param memberManage  사원 시퀀스
     * @param increaseOrDecreaseValue  증감값
     */

    public void putPlusHoliday(MemberManage memberManage, float increaseOrDecreaseValue) {
        HolidayCount holidayCount = holidayCountRepository.findById(memberManage.getId()).orElseThrow(CMissingDataException::new);


        if (holidayCount.getDateCreate() != LocalDate.now())

        holidayCount.plusCountTotal(increaseOrDecreaseValue);
        holidayCountRepository.save(holidayCount);
    }


    /**
     * 휴가 신청내역 가저오기
     *
     * @param holidayHistoryId
     * @return 휴가 신청내역 정보
     */
    public HolidayItem getHoliday(long holidayHistoryId) {
        HolidayHistory holidayHistory = holidayHistoryRepository.findById(holidayHistoryId).orElseThrow(CMissingDataException::new);
        MemberManage memberManage = memberRepository.findById(holidayHistoryId).orElseThrow(CMissingDataException::new);
        return new HolidayItem.HolidayItemBuilder(memberManage, holidayHistory).build();
    }
}
