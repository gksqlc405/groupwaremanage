package com.chb.groupwareManage.service.loginService;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.exception.*;
import com.chb.groupwareManage.model.login.LoginScreenItem;
import com.chb.groupwareManage.model.login.PasswordUpdateRequest;
import com.chb.groupwareManage.model.login.MemberLoginRequest;
import com.chb.groupwareManage.model.login.MemberLoginResponse;
import com.chb.groupwareManage.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;

    // 로그인
    public MemberLoginResponse login(MemberLoginRequest request, boolean isAdmin) {
        MemberManage memberManage = memberRepository.findByUsernameAndIsAdmin(request.getUsername(), isAdmin).orElseThrow(CNoUserNameException::new);

        if (!memberManage.getPassword().equals(request.getPassword())) throw new CNoPasswordException();

        if (!memberManage.getIsEnabled()) throw new CNoIsEnabledException();

        return new MemberLoginResponse.MemberLoginResponseBuilder(memberManage).build();
    }


    // 비밀번호 변경
    public void putPassword(long memberId, PasswordUpdateRequest request) {
        MemberManage memberManage = memberRepository.findByIdAndPassword(memberId, request.getPassword()).orElseThrow(CMissingDataException::new);

        if (!memberManage.getPassword().equals(request.getPassword())) throw new CNoPasswordException();

        if (!request.getNewPassword().equals(request.getCheckPassword())) throw new CNoCheckPasswordException();


        memberManage.putPassWord(request);
        memberRepository.save(memberManage);

    }


}
