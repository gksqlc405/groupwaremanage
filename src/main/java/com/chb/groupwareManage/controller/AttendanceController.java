package com.chb.groupwareManage.controller;

import com.chb.groupwareManage.enums.AttendanceStatus;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.SingleResult;
import com.chb.groupwareManage.model.user.AttendanceItem;
import com.chb.groupwareManage.model.user.AttendanceCount;
import com.chb.groupwareManage.model.user.AttendanceResponse;
import com.chb.groupwareManage.model.user.AttendanceTestModel;
import com.chb.groupwareManage.service.ResponseService;
import com.chb.groupwareManage.service.UserService.AttendanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Api(tags = "근태관리 (사용자)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/attendance")
public class AttendanceController {
    private final AttendanceService attendanceService;

    @ApiOperation(value = "근태 상태 변경")
    @PutMapping("/status/{attendanceStatus}/member-id/{memberId}")
    public SingleResult<AttendanceResponse> doStatusChange(
            @PathVariable AttendanceStatus attendanceStatus,
            @PathVariable long memberId

    ) {
        return ResponseService.getSingleResult(attendanceService.doAttendanceChange(memberId, attendanceStatus));

    }

    @ApiOperation(value = "근태 상태 가저오기")
    @GetMapping("/status/member-id/{memberId}")
    public SingleResult<AttendanceResponse> getStatus(@PathVariable long memberId) {
        return ResponseService.getSingleResult(attendanceService.getCurrentStatus(memberId));
    }


    @ApiOperation(value = "출퇴근 날짜 확인")
    @GetMapping("/search/{searchId}")
    public ListResult<AttendanceTestModel> getAttendance(@PathVariable long searchId,
                                                         @RequestParam(value = "dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
                                                         @RequestParam(value = "dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateEnd) {
        return ResponseService.getListResult(attendanceService.getAttendance(dateStart, dateEnd,searchId), true);
    }



    @ApiOperation(value = "근무일수 확인")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/attendance/{memberId}/year/{year}/month/{month}")
    public SingleResult<AttendanceCount> getCountByMyYearMonth(@PathVariable long memberId, @PathVariable int year, @PathVariable  int month) {
        return ResponseService.getSingleResult(attendanceService.getCountByMyYearMonth(memberId, year, month));
    }
}
