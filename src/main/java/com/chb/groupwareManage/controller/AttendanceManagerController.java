package com.chb.groupwareManage.controller;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.manager.AttendanceManagerItem;
import com.chb.groupwareManage.service.ManagerService.AttendanceManagerService;
import com.chb.groupwareManage.service.ManagerService.MemberManagerService;
import com.chb.groupwareManage.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "근태관리 (관리자)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/attendance-manager")
public class AttendanceManagerController {
    private final AttendanceManagerService attendanceManagerService;


    @ApiOperation(value = "금일 출퇴근 현황")
    @GetMapping("/attendance")
    public ListResult<AttendanceManagerItem> getAttendances() {
        return ResponseService.getListResult(attendanceManagerService.getAttendances(), true);
    }
}
