package com.chb.groupwareManage.controller;

import com.chb.groupwareManage.model.CommonResult;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.SingleResult;
import com.chb.groupwareManage.model.manager.BannerUrlRequest;
import com.chb.groupwareManage.model.manager.BannerUrlUpdateRequest;
import com.chb.groupwareManage.model.user.BannerUrlItem;
import com.chb.groupwareManage.service.ManagerService.BannerManagerService;
import com.chb.groupwareManage.service.ResponseService;
import com.chb.groupwareManage.service.UserService.BannerUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "배너 url")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/banner")
public class BannerUrlController {
    private final BannerManagerService bannerManagerService;
    private final BannerUserService bannerUserService;


    @ApiOperation(value = "배너 url 등록")
    @PostMapping("/new")
    public CommonResult setBanner(@RequestBody @Valid BannerUrlRequest request) {
        bannerManagerService.setBanner(request);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "배너 url 하나 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bannerId", value = "배너 시퀀스", required = true)
    })
    @GetMapping("/{bannerId}")
    public SingleResult<BannerUrlItem> getBanner(@PathVariable long bannerId) {
        return ResponseService.getSingleResult(bannerUserService.getBanner(bannerId));
    }


    @ApiOperation(value = "배너 url  모두가저오기")
    @GetMapping("/all")
    public ListResult<BannerUrlItem> getBanners() {
        return ResponseService.getListResult(bannerUserService.getBanners(), true);
    }


    @ApiOperation(value = "배너 url 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "배너 수정 시퀀스", required = true)
    })
    @PutMapping("id{id}")
    public CommonResult putBanner(@PathVariable long id, @RequestBody @Valid BannerUrlUpdateRequest request) {
        bannerManagerService.putBanner(id,request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "배너 url 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "배너 삭제 시퀀스", required = true)
    })
    @DeleteMapping("id{id}")
    public CommonResult delBanner(@PathVariable long id) {
        bannerManagerService.delBanner(id);
        return ResponseService.getSuccessResult();
    }


}
