package com.chb.groupwareManage.controller;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.enums.Department;
import com.chb.groupwareManage.enums.Position;
import com.chb.groupwareManage.model.CommonResult;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.SingleResult;
import com.chb.groupwareManage.model.manager.IsAdminUpdateRequest;
import com.chb.groupwareManage.model.manager.MemberItem;
import com.chb.groupwareManage.model.manager.MemberJoinRequest;
import com.chb.groupwareManage.model.manager.MemberManagerUpdateRequest;
import com.chb.groupwareManage.service.ManagerService.HolidayManagerService;
import com.chb.groupwareManage.service.ManagerService.MemberManagerService;
import com.chb.groupwareManage.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "사원 정보 관리 (관리자)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberManageController {
    private final MemberManagerService memberManagerService;
    private final HolidayManagerService holidayManagerService;


    @ApiOperation(value = "사원 정보 등록")
    @PostMapping("/new}")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest request) {
        MemberManage memberManage = memberManagerService.setMember(request);
        holidayManagerService.setHoliday(memberManage, memberManage.getDateIn());
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원정보 하나 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/id/{id}")
    public SingleResult<MemberItem> getMember(@PathVariable long id) {
        return ResponseService.getSingleResult(memberManagerService.getMember(id));
    }


    @ApiOperation(value = "사원정보 모두 가저오기")
    @GetMapping("/all")
    public ListResult<MemberItem> getMembers(boolean isEnabled) {
        return ResponseService.getListResult(memberManagerService.getMembers(isEnabled),true);
    }


    @ApiOperation(value = "사원 직급 가저오기")
    @GetMapping("/position")
    public ListResult<MemberItem> getPosition(Position position) {
        return ResponseService.getListResult(memberManagerService.getPosition(position), true);
    }


    @ApiOperation(value = "부서 가저오기")
    @GetMapping("/department")
    public ListResult<MemberItem> getDepartment(Department department) {
        return ResponseService.getListResult(memberManagerService.getDepartment(department), true);
    }

    @ApiOperation(value = "사원 이름 가저오기")
    @GetMapping("/name")
    public ListResult<MemberItem> getName (String name) {
        return ResponseService.getListResult(memberManagerService.getName(name), true);
    }

    @ApiOperation(value = "사원 정보 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "사원 시퀀스", required = true)
    })
    @PutMapping("/member/{memberId}")
    public CommonResult putMemberInfo(@PathVariable long memberId, @RequestBody @Valid MemberManagerUpdateRequest request) {
        memberManagerService.putMemberInfo(memberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "관리자 권한부여")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "isAdminId", value = "사원 시퀀스", required = true)
    })
    @PutMapping("/isAdmin/{isAdminId}")
    public CommonResult putIsAdmin(@PathVariable long isAdminId, @RequestBody @Valid IsAdminUpdateRequest request) {
        memberManagerService.putIsAdmin(isAdminId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원 퇴사")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dateOutId", value = "사원 시퀀스", required = true)
    })
    @DeleteMapping("/{dateOutId}")
    public CommonResult putDateOutId(@PathVariable long dateOutId) {
        memberManagerService.putDateOut(dateOutId);
        return ResponseService.getSuccessResult();
    }



}
