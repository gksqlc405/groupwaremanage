package com.chb.groupwareManage.controller;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.model.CommonResult;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.SingleResult;
import com.chb.groupwareManage.model.user.HolidayRegistrationRequest;
import com.chb.groupwareManage.model.user.HolidayStatusItem;
import com.chb.groupwareManage.model.user.HolidayUseItem;
import com.chb.groupwareManage.service.ManagerService.MemberManagerService;
import com.chb.groupwareManage.service.ResponseService;
import com.chb.groupwareManage.service.UserService.HolidayUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "휴가신청 (사용자)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/approval")
public class HolidayUserController {
    private final HolidayUserService holidayUserService;
    private final MemberManagerService memberManagerService;

    @ApiOperation(value = "휴가 신청")
    @PostMapping("/post/{memberId}")
    public CommonResult setApproval(@PathVariable long memberId,@RequestBody @Valid HolidayRegistrationRequest request) {
        MemberManage memberManage = memberManagerService.getMemberId(memberId);
        holidayUserService.setHolidayApply(memberManage, request);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "휴가 사용 현황")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/holidayUse/member-id/{memberId}/year/{year}")
    public SingleResult<HolidayUseItem> getHolidayUse(@PathVariable long memberId, @PathVariable int year ) {
        return ResponseService.getSingleResult(holidayUserService.getHolidayUse(memberId, year));
    }

    @ApiOperation(value = "휴가 신청 현황")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/holidayStatus/{memberId}/year/{year}")
    public ListResult<HolidayStatusItem> getHolidayStatus(@PathVariable long memberId, @PathVariable int year) {
        MemberManage memberManage = memberManagerService.getMemberId(memberId);
        return ResponseService.getListResult(holidayUserService.getHolidayStatus(memberManage, year), true);
    }


}
