package com.chb.groupwareManage.controller;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.model.CommonResult;
import com.chb.groupwareManage.model.SingleResult;
import com.chb.groupwareManage.model.login.LoginScreenItem;
import com.chb.groupwareManage.model.login.PasswordUpdateRequest;
import com.chb.groupwareManage.model.login.MemberLoginRequest;
import com.chb.groupwareManage.model.login.MemberLoginResponse;
import com.chb.groupwareManage.model.user.UserWithdrawalRequest;
import com.chb.groupwareManage.service.ManagerService.MemberManagerService;
import com.chb.groupwareManage.service.ResponseService;
import com.chb.groupwareManage.service.loginService.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "로그인 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;


    @ApiOperation(value = "관리자 로그인")
    @PostMapping("/manager")
    public SingleResult<MemberLoginResponse> managerLogin(@RequestBody @Valid MemberLoginRequest request) {
        return ResponseService.getLoginResult(loginService.login(request, true));
    }


    @ApiOperation(value = "사용자 로그인")
    @PostMapping("/memberLogin")
    public SingleResult<MemberLoginResponse> memberLogin(@RequestBody @Valid MemberLoginRequest request) {
        return ResponseService.getLoginResult(loginService.login(request, false));
    }

    @ApiOperation(value = "비밀번호 변겅")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "사원 시퀀스", required = true)
    })
    @PutMapping("/password/{memberId}")
    public CommonResult putPassword(@PathVariable long memberId, @RequestBody @Valid PasswordUpdateRequest request) {
        loginService.putPassword(memberId, request);
        return ResponseService.getPasswordSuccessResult();
    }



}
