package com.chb.groupwareManage.controller;

import com.chb.groupwareManage.model.CommonResult;
import com.chb.groupwareManage.model.SingleResult;
import com.chb.groupwareManage.model.user.MyPagePhoneUpdate;
import com.chb.groupwareManage.model.user.UserMyPageItem;
import com.chb.groupwareManage.model.user.UserWithdrawalRequest;
import com.chb.groupwareManage.service.ResponseService;
import com.chb.groupwareManage.service.UserService.UserMyPageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "사원 정보 (사용자)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/user")
public class MemberUserController {
    private final UserMyPageService userMyPageService;

    @ApiOperation(value = "MyPage 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "myPageId", value = "사원 시퀀스", required = true)
    })
    @GetMapping("/myPage/{memberId}")
    public SingleResult<UserMyPageItem> getMyPage(@PathVariable long memberId) {
        return ResponseService.getSingleResult(userMyPageService.getMyPage(memberId));
    }


    @ApiOperation(value = "사원 탈퇴")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "withdrawalId", value = "사원 시퀀스", required = true)
    })
    @DeleteMapping("/member/{memberId}")
    public CommonResult putWithdrawal(@PathVariable long memberId, @RequestBody @Valid UserWithdrawalRequest request) {
        userMyPageService.putWithdrawal(memberId, request);
        return ResponseService.getWithdrawal();
    }

    @ApiOperation(value = "회원 폰번호 수정")
    @PutMapping("/phone/{memberId}")
    public CommonResult putPhoneNumber(@PathVariable long memberId, @RequestBody @Valid MyPagePhoneUpdate myPagePhoneUpdate) {
        userMyPageService.putPhoneNumber(memberId, myPagePhoneUpdate);
        return ResponseService.getSuccessResult();
    }
}
