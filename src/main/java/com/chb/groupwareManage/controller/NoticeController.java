package com.chb.groupwareManage.controller;

import com.chb.groupwareManage.model.CommonResult;
import com.chb.groupwareManage.model.ListResult;
import com.chb.groupwareManage.model.SingleResult;
import com.chb.groupwareManage.model.manager.NoticeRequest;
import com.chb.groupwareManage.model.manager.NoticeUpdateRequest;
import com.chb.groupwareManage.model.user.NoticeItem;
import com.chb.groupwareManage.service.ManagerService.NoticeManagerService;
import com.chb.groupwareManage.service.ResponseService;
import com.chb.groupwareManage.service.UserService.NoticeUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "공지사항")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/notice")
public class NoticeController {
    private final NoticeManagerService noticeManagerService;
    private final NoticeUserService noticeUserService;

    @ApiOperation(value = "공지사항 내용 등록")
    @PostMapping("/notice")
    public CommonResult setNotice(@RequestBody @Valid NoticeRequest request) {
        noticeManagerService.setNotice(request);
        return ResponseService.getSuccessResult();

    }


    @ApiOperation(value = "공지사항 한개 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "공지사항 시퀀스", required = true)
    })
    @GetMapping("/id/{id}")
    public SingleResult<NoticeItem> getNotice(@PathVariable long id) {
        return ResponseService.getSingleResult(noticeUserService.getNotice(id));
    }
    @ApiOperation(value = "공지사항 모두 가저오기")
    @GetMapping("/all")
    public ListResult<NoticeItem> getNotices() {
        return ResponseService.getListResult(noticeUserService.getNotices(),true);
    }

    @ApiOperation(value = "공지사항 내용 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "updateId", value = "공지사항 시퀀스", required = true)
    })
    @PutMapping("/upDate/{updateId}")
    public CommonResult putNotice(@PathVariable long updateId, @RequestBody @Valid NoticeUpdateRequest request) {
        noticeManagerService.putNotice(updateId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "공지사항 내용 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "delId", value = "공지사항 시퀀스", required = true)
    })
    @DeleteMapping("/{delId}")
    public CommonResult delNotice(@PathVariable long delId) {
        noticeManagerService.delNotice(delId);
        return ResponseService.getSuccessResult();
    }

}
