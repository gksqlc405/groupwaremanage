package com.chb.groupwareManage.controller;

import com.chb.groupwareManage.entity.HolidayHistory;
import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.enums.ApprovalStatus;
import com.chb.groupwareManage.model.CommonResult;
import com.chb.groupwareManage.model.SingleResult;
import com.chb.groupwareManage.model.manager.HolidayItem;
import com.chb.groupwareManage.service.ManagerService.HolidayManagerService;
import com.chb.groupwareManage.service.ManagerService.MemberManagerService;
import com.chb.groupwareManage.service.ResponseService;
import com.chb.groupwareManage.service.UserService.HolidayUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Api(tags = "휴가 정보 (관리자)")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/holiday-count")
public class HolidayManagerController {
    private final HolidayManagerService holidayManagerService;
    private final MemberManagerService memberManagerService;
    private final HolidayUserService holidayUserService;


    @ApiOperation(value = "휴가 승인 ")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "historyId", value = "history 시퀀스", required = true)
    })
    @PutMapping("/history/{historyId}")
    public CommonResult putHoliday(@PathVariable long historyId, boolean isManager ) {
        HolidayHistory holidayHistory = holidayManagerService.getHistoryId(historyId);
        holidayManagerService.putHolidayCount(holidayHistory, isManager);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "연차 추가")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "plusId", value = "사원 시퀀스", required = true)
    })
    @PutMapping("/plus/{plusId}")
    public CommonResult putPlusHolidayPlus(@PathVariable long plusId, float increaseOrDecreaseValue ) {
        MemberManage memberManage =memberManagerService.getMemberId(plusId);
        holidayManagerService.putPlusHoliday(memberManage, increaseOrDecreaseValue);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "휴가신청서 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "holidayId", value = "사원시퀀스", required = true)
    })
    @GetMapping("/holiday/{holidayId}")
    public SingleResult<HolidayItem> getHoliday(@PathVariable long holidayId) {
        return ResponseService.getSingleResult(holidayManagerService.getHoliday(holidayId));
    }

}
