package com.chb.groupwareManage.model.manager;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.enums.Department;
import com.chb.groupwareManage.enums.Position;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {

    @ApiModelProperty(notes = "사원 시퀀스")
    private Long memberId;
    @ApiModelProperty(notes = "아이디")
    private String username;
    @ApiModelProperty(notes = "비밀번호")
    private String password;
    @ApiModelProperty(notes = "이름")
    private String name;
    @ApiModelProperty(notes = "전화번호")
    private String phone;
    @ApiModelProperty(notes = "부서")
    private String department;
    @ApiModelProperty(notes = "직급")
    private String position;
    @ApiModelProperty(notes = "주소")
    private String address;
    @ApiModelProperty(notes = "생년월일")
    private LocalDate birthday;
    @ApiModelProperty(notes = "입사일")
    private LocalDate dateIn;
    @ApiModelProperty(notes = "퇴사여부")
    private Boolean isEnabled;
    @ApiModelProperty(notes = "관리자 권한 여부")
    private Boolean isAdmin;

    private MemberItem(MemberItemBuilder builder) {
        this.memberId = builder.memberId;
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.phone = builder.phone;
        this.department = builder.department;
        this.position = builder.position;
        this.address = builder.address;
        this.birthday = builder.birthday;
        this.dateIn = builder.dateIn;
        this.isEnabled = builder.isEnabled;
        this.isAdmin = builder.isAdmin;
    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {

        private final Long memberId;
        private final String username;
        private final String password;
        private final String name;
        private final String phone;
        private final String department;
        private final String position;
        private final String address;
        private final LocalDate birthday;
        private final LocalDate dateIn;
        private final Boolean isEnabled;
        private final Boolean isAdmin;

        public MemberItemBuilder(MemberManage memberManage) {
            this.memberId = memberManage.getId();
            this.username = memberManage.getUsername();
            this.password = memberManage.getPassword();
            this.name = memberManage.getName();
            this.phone = memberManage.getPhone();
            this.department = memberManage.getDepartment().getName();
            this.position = memberManage.getPosition().getName();
            this.address = memberManage.getAddress();
            this.birthday = memberManage.getBirthday();
            this.dateIn = memberManage.getDateIn();
            this.isEnabled = memberManage.getIsEnabled();
            this.isAdmin = memberManage.getIsAdmin();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
