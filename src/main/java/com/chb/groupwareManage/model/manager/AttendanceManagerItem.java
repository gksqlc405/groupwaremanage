package com.chb.groupwareManage.model.manager;

import com.chb.groupwareManage.entity.AttendanceManage;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceManagerItem {

    private String name;

    private String attendanceType;

    private LocalTime dateAttendance;

    private LocalTime dateLeaveWork;

    private LocalTime dateLeaveEarly;


    private AttendanceManagerItem(AttendanceManagerItemBuilder builder) {
        this.name = builder.name;
        this.attendanceType = builder.attendanceType;
        this.dateAttendance = builder.dateAttendance;
        this.dateLeaveWork = builder.dateLeaveWork;
        this.dateLeaveEarly = builder.dateLeaveEarly;

    }

    public static class AttendanceManagerItemBuilder implements CommonModelBuilder<AttendanceManagerItem> {

        private final String name;

        private final String attendanceType;

        private final LocalTime dateAttendance;

        private final LocalTime dateLeaveWork;

        private final LocalTime dateLeaveEarly;

        public AttendanceManagerItemBuilder(AttendanceManage attendanceManage) {
            this.name = attendanceManage.getMemberManage().getName();
            this.attendanceType = attendanceManage.getAttendanceType().getName();
            this.dateAttendance = attendanceManage.getDateAttendance();
            this.dateLeaveWork = attendanceManage.getDateLeaveWork();
            this.dateLeaveEarly = attendanceManage.getDateLeaveEarly();

        }


        @Override
        public AttendanceManagerItem build() {
            return new AttendanceManagerItem(this);
        }
    }

}
