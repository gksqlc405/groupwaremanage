package com.chb.groupwareManage.model.manager;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class IsAdminUpdateRequest {

    @NotNull
    @ApiModelProperty(notes = "관리자 권한 여부")
    private Boolean isAdmin;
}
