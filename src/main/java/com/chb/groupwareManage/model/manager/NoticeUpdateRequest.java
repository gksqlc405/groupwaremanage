package com.chb.groupwareManage.model.manager;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class NoticeUpdateRequest {

    @NotNull
    @Length(min = 5, max = 150)
    @ApiModelProperty(notes = "공지사항 내용")
    private String note;
}
