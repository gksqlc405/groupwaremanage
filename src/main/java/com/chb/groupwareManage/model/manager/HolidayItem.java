package com.chb.groupwareManage.model.manager;

import com.chb.groupwareManage.entity.HolidayHistory;
import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.enums.ApprovalStatus;
import com.chb.groupwareManage.enums.HolidayType;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidayItem {

    @ApiModelProperty(notes = "사원 이름")
    private String name;

    @ApiModelProperty(notes = "휴가 신청 이유")
    private String reason;

    @ApiModelProperty(notes = "휴가 신청 종류")
    private String holidayType;

    @ApiModelProperty(notes = "휴가 시작일")
    private LocalDate dateHolidayStart;

    @ApiModelProperty(notes = "휴가 끝일")
    private LocalDate dateHolidayEnd;

    @ApiModelProperty(notes = "휴가 신청 상태")
    private String approvalStatus;

    @ApiModelProperty(notes = "휴가 등록기간")
    private LocalDate dateCreate;


    private HolidayItem(HolidayItemBuilder builder) {
        this.name = builder.name;
        this.reason = builder.reason;
        this.holidayType = builder.holidayType;
        this.dateHolidayStart = builder.dateHolidayStart;
        this.dateHolidayEnd = builder.dateHolidayEnd;
        this.approvalStatus = builder.approvalStatus;
        this.dateCreate = builder.dateCreate;

    }

    public static class  HolidayItemBuilder implements CommonModelBuilder<HolidayItem> {
        private final String name;
        private final String reason;
        private final String holidayType;
        private final LocalDate dateHolidayStart;
        private final LocalDate dateHolidayEnd;
        private final String approvalStatus;
        private final LocalDate dateCreate;

        public HolidayItemBuilder(MemberManage memberManage, HolidayHistory holidayHistory) {
            this.name = memberManage.getName();
            this.reason = holidayHistory.getReason();
            this.holidayType = holidayHistory.getHolidayType().getName();
            this.dateHolidayStart =holidayHistory.getDateHolidayStart();
            this.dateHolidayEnd = holidayHistory.getDateHolidayEnd();
            this.approvalStatus = holidayHistory.getApprovalStatus().getName();
            this.dateCreate = holidayHistory.getDateCreate();
        }


        @Override
        public HolidayItem build() {
            return new HolidayItem(this);
        }
    }




}
