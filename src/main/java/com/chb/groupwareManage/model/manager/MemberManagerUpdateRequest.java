package com.chb.groupwareManage.model.manager;

import com.chb.groupwareManage.enums.Department;
import com.chb.groupwareManage.enums.Position;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberManagerUpdateRequest {

    @NotNull
    @Length(min = 1, max = 10)
    @ApiModelProperty(notes = "사원 이름", required = true)
    private String name;

    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(notes = "사원 전화번호", required = true)
    private String phone;

    @NotNull
    @ApiModelProperty(notes = "부서", required = true)
    private Department department;

    @NotNull
    @ApiModelProperty(notes = "직급", required = true)
    private Position position;

    @NotNull
    @Length(min = 5, max = 50)
    @ApiModelProperty(notes = "사원 주소", required = true)
    private String address;
}
