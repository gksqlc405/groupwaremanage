package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserMyPageItem {

    @ApiModelProperty(notes = "이름")
    private String name;

    @ApiModelProperty(notes = "전화번호")
    private String phone;

    @ApiModelProperty(notes = "부서")
    private String department;

    @ApiModelProperty(notes = "직급")
    private String position;

    private UserMyPageItem(UserMyPageItemBuilder builder) {
        this.name = builder.name;
        this.phone = builder.phone;
        this.department = builder.department;
        this.position = builder.position;

    }

    public static class UserMyPageItemBuilder implements CommonModelBuilder<UserMyPageItem> {
        private final String name;
        private final String phone;
        private final String department;
        private final String position;

        public UserMyPageItemBuilder(MemberManage memberManage) {
            this.name = memberManage.getName();
            this.phone = memberManage.getPhone();
            this.department = memberManage.getDepartment().getName();
            this.position = memberManage.getPosition().getName();

        }

        @Override
        public UserMyPageItem build() {
            return new UserMyPageItem(this);
        }
    }
}
