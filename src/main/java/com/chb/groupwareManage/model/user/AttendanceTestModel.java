package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.entity.Attendance;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceTestModel {

    private String attendanceStatus;
    private LocalDate dateWork;

    private String timeStart;
    private String timeLeaveEarly;
    private String timeLeave;

    private AttendanceTestModel(AttendanceTestModelBuilder builder) {
        this.attendanceStatus = builder.attendanceStatus;
        this.dateWork = builder.dateWork;
        this.timeStart = builder.timeStart;
        this.timeLeaveEarly = builder.timeLeaveEarly;
        this.timeLeave = builder.timeLeave;
    }

    public static class AttendanceTestModelBuilder implements CommonModelBuilder<AttendanceTestModel> {

        private final String attendanceStatus;
        private final LocalDate dateWork;

        private final String timeStart;

        private final String timeLeaveEarly;

        private final String timeLeave;

        public AttendanceTestModelBuilder(Attendance attendance) {
            this.attendanceStatus = attendance.getAttendanceStatus().getName();
            this.dateWork = attendance.getDateWork();
            this.timeStart = attendance.getTimeStart().toString();
            this.timeLeaveEarly = attendance.getTimeLeaveEarly() == null ? "-" : attendance.getTimeLeaveEarly().toString();
            this.timeLeave = attendance.getTimeLeave() == null? "-" : attendance.getTimeLeave().toString();
        }

        @Override
        public AttendanceTestModel build() {
            return new AttendanceTestModel(this);
        }
    }
}


