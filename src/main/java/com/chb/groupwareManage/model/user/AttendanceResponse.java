package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.entity.Attendance;
import com.chb.groupwareManage.enums.AttendanceStatus;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceResponse {

    // 한국어로 출근/조퇴/퇴근
    private String attendanceStatusName;

    // ENum - key값
    private String attendanceStatus;



    private AttendanceResponse(AttendanceResponseBuilder builder) {
        this.attendanceStatusName = builder.attendanceStatusName;
        this.attendanceStatus = builder.attendanceStatus;
    }

    public static class AttendanceResponseBuilder implements CommonModelBuilder<AttendanceResponse> {

        private final String attendanceStatusName;
        private final String attendanceStatus;

        public AttendanceResponseBuilder(Attendance attendance) {
            this.attendanceStatusName = attendance.getAttendanceStatus().getName();
            this.attendanceStatus = attendance.getAttendanceStatus().toString();

        }


        @Override
        public AttendanceResponse build() {
            return new AttendanceResponse(this);
        }
    }

    private AttendanceResponse(AttendanceResponseNoneBuilder builder) {
        this.attendanceStatusName = builder.attendanceStatusName;
        this.attendanceStatus = builder.attendanceStatus;

    }

    public static class AttendanceResponseNoneBuilder implements CommonModelBuilder<AttendanceResponse> {
        private final String attendanceStatusName;
        private final String attendanceStatus;



        public AttendanceResponseNoneBuilder() {
            this.attendanceStatusName = AttendanceStatus.NONE.getName();
            this.attendanceStatus = AttendanceStatus.NONE.toString();
        }

        @Override
        public AttendanceResponse build() {
            return new AttendanceResponse(this);
        }
    }
}
