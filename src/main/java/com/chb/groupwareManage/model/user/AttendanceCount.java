package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceCount {

    private Long countResult;

    private AttendanceCount(AttendanceLongBuilder builder) {
        this.countResult = builder.countResult;
    }

    public static class AttendanceLongBuilder implements CommonModelBuilder<AttendanceCount> {
        private final Long countResult;


        public AttendanceLongBuilder(Long countResult) {
            this.countResult = countResult;
        }

        @Override
        public AttendanceCount build() {
            return new AttendanceCount(this);
        }
    }
}
