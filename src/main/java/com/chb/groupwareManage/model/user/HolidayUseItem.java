package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.entity.HolidayCount;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidayUseItem {

    @ApiModelProperty(notes = "총 연차 갯수")
    private Float countTotal;

    @ApiModelProperty(notes = "사용 연차 갯수")
    private Float countUse;

    @ApiModelProperty(notes = "잔여 연차 갯수")
    private Float countRemaining;

    private HolidayUseItem(HolidayUseItemBuilder builder) {
        this.countTotal = builder.countTotal;
        this.countUse = builder.countUse;
        this.countRemaining = builder.countRemaining;


    }

    public static class HolidayUseItemBuilder implements CommonModelBuilder<HolidayUseItem> {
        private final Float countTotal;
        private final Float countUse;
        private final Float countRemaining;


        public HolidayUseItemBuilder(HolidayCount holidayCount) {
            this.countTotal = holidayCount.getCountTotal();
            this.countUse = holidayCount.getCountUse();
            this.countRemaining = holidayCount.getCountRemaining();

        }


        @Override
        public HolidayUseItem build() {
            return new HolidayUseItem(this);
        }
    }

}

