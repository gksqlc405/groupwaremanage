package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.enums.ApprovalStatus;
import com.chb.groupwareManage.enums.HolidayType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class HolidayRegistrationRequest {

    @NotNull
    @Length(min = 3, max = 100)
    @ApiModelProperty(notes = "휴가 신청 이유")
    private String reason;

    @NotNull
    @ApiModelProperty(notes = "휴가 신청 종류")
    @Enumerated(value = EnumType.STRING)
    private HolidayType holidayType;

    @NotNull
    @ApiModelProperty(notes = "휴가 시작일")
    private LocalDate dateHolidayStart;

    @NotNull
    @ApiModelProperty(notes = "휴가 끝일")
    private LocalDate dateHolidayEnd;


    @NotNull
    @ApiModelProperty(notes = "증감값")
    private float increaseOrDecreaseValue;
}
