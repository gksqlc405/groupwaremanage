package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.entity.BannerUrl;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BannerUrlItem {

    private String url;

    private LocalDateTime dateCreate;

    private LocalDateTime dateUpdate;

    private BannerUrlItem(BannerUrlItemBuilder builder) {
        this.url = builder.url;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;

    }


    public static class BannerUrlItemBuilder implements CommonModelBuilder<BannerUrlItem> {
        private final String url;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public BannerUrlItemBuilder(BannerUrl bannerUrl) {
            this.url = bannerUrl.getUrl();
            this.dateCreate = bannerUrl.getDateCreate();
            this.dateUpdate = bannerUrl.getDatUpdate();
        }

        @Override
        public BannerUrlItem build() {
            return new BannerUrlItem(this);
        }
    }
}
