package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.entity.AttendanceManage;
import com.chb.groupwareManage.enums.AttendanceStatus;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceTypeResponse {

    @Enumerated(value = EnumType.STRING)
    @ApiModelProperty(notes = "근태상태")
    private AttendanceStatus attendanceType;


    private AttendanceTypeResponse(AttendanceTypeResponseBuilder builder) {
        this.attendanceType = builder.attendanceType;

    }

    public static class AttendanceTypeResponseBuilder implements CommonModelBuilder<AttendanceTypeResponse> {
        private final AttendanceStatus attendanceType;



        public AttendanceTypeResponseBuilder(AttendanceManage attendanceManage) {
            this.attendanceType = attendanceManage.getAttendanceType();
        }

        @Override
        public AttendanceTypeResponse build() {
            return new AttendanceTypeResponse(this);
        }
    }
}
