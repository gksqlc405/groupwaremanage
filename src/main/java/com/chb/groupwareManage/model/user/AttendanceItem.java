package com.chb.groupwareManage.model.user;
import com.chb.groupwareManage.entity.Attendance;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceItem {


    private String attendanceStatus;
    private LocalDate dateWork;

    private AttendanceItem(AttendanceItemBuilder builder) {
        this.attendanceStatus = builder.attendanceStatus;
        this.dateWork = builder.dateWork;
    }

    public static class AttendanceItemBuilder implements CommonModelBuilder<AttendanceItem> {

        private final String attendanceStatus;
        private final LocalDate dateWork;

        public AttendanceItemBuilder(Attendance attendance) {
            this.attendanceStatus = attendance.getAttendanceStatus().getName();
            this.dateWork = attendance.getDateWork();
        }

        @Override
        public AttendanceItem build() {
            return new AttendanceItem(this);
        }
    }
}
