package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.entity.Notice;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NoticeItem {

    private String name;

    private LocalDateTime dateCreate;

    private LocalDateTime dateUpdate;


    private NoticeItem(NoticeItemBuilder builder) {
        this.name = builder.name;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;

    }

    public static class NoticeItemBuilder implements CommonModelBuilder<NoticeItem> {
        private final String name;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public NoticeItemBuilder(Notice notice) {
            this.name = notice.getNote();
            this.dateCreate = notice.getDateCreate();
            this.dateUpdate = notice.getDateUpdate();
        }

        @Override
        public NoticeItem build() {
            return new NoticeItem(this);
        }
    }

}
