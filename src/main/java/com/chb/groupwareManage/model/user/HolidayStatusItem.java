package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.entity.HolidayHistory;
import com.chb.groupwareManage.enums.ApprovalStatus;
import com.chb.groupwareManage.enums.HolidayType;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HolidayStatusItem {


    @ApiModelProperty(notes = "휴가 신청 종류")
    private String holidayType;


    @ApiModelProperty(notes = "휴가 기간")
    private String dateHoliday;

    @ApiModelProperty(notes = "승인 상태")
    private String approvalStatus;

    @ApiModelProperty(notes = "요청 시간")
    private LocalDate dateCreate;


    private HolidayStatusItem(HolidayStatusItemBuilder builder) {
        this.holidayType = builder.holidayType;
        this.dateHoliday = builder.dateHoliday;
        this.approvalStatus = builder.approvalStatus;
        this.dateCreate = builder.dateCreate;

    }

    public static class HolidayStatusItemBuilder implements CommonModelBuilder<HolidayStatusItem> {
        private final String holidayType;
        private final String dateHoliday;
        private final String approvalStatus;
        private final LocalDate dateCreate;

        public HolidayStatusItemBuilder(HolidayHistory holidayHistory) {
            this.holidayType = holidayHistory.getHolidayType().getName();
            this.dateHoliday = holidayHistory.getDateHolidayStart() + " ~ " +  holidayHistory.getDateHolidayEnd();
            this.approvalStatus = holidayHistory.getApprovalStatus().getName();
            this.dateCreate = holidayHistory.getDateCreate();
        }

        @Override
        public HolidayStatusItem build() {
            return new HolidayStatusItem(this);
        }
    }

}
