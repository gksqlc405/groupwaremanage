package com.chb.groupwareManage.model.user;

import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MyPagePhoneUpdate {


    @NotNull
    @ApiModelProperty(notes = "전화번호")
    private String phone;

}
