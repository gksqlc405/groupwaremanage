package com.chb.groupwareManage.model.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UserWithdrawalRequest {

    @NotNull
    @Length(min = 5, max = 20)
    @ApiModelProperty(notes = "비밀번호", required = true)
    private String password;

    @NotNull
    @Length(min = 5, max = 20)
    @ApiModelProperty(notes = "비밀번호 재확인", required = true)
    private String checkPassword;


}
