package com.chb.groupwareManage.model.login;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginScreenItem {


    @ApiModelProperty(notes = "현재 비밀번호", required = true)
    private String password;

    @ApiModelProperty(notes = "새 비밀번호", required = true)
    private String newPassword;

    @ApiModelProperty(notes = "새 비밀번호 확인", required = true)
    private String checkPassword;

    private LoginScreenItem(LoginScreenItemBuilder builder) {
        this.password = builder.password;
        this.newPassword = builder.newPassword;
        this.checkPassword = builder.checkPassword;

    }


    public static class LoginScreenItemBuilder implements CommonModelBuilder<LoginScreenItem> {
        private final String password;
        private final String newPassword;
        private final String checkPassword;

        public LoginScreenItemBuilder(String password ,String newPassword, String checkPassword) {
            this.password = password + "현재 비밀번호";
            this.newPassword = newPassword + "새 비밀번호";
            this.checkPassword = checkPassword + "새 비밀번호 확인";
        }

        @Override
        public LoginScreenItem build() {
            return new LoginScreenItem(this);
        }
    }

}
