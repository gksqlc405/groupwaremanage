package com.chb.groupwareManage.model.login;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberLoginResponse {

    @ApiModelProperty(notes = "사원 시퀀스")
    private Long memberId;




    private MemberLoginResponse(MemberLoginResponseBuilder builder) {
        this.memberId = builder.memberId;


    }

    public static class MemberLoginResponseBuilder implements CommonModelBuilder<MemberLoginResponse> {

        private final Long memberId;


        public MemberLoginResponseBuilder(MemberManage memberManage) {
            this.memberId = memberManage.getId();

        }

        @Override
        public MemberLoginResponse build() {
            return new MemberLoginResponse(this);
        }
    }
}
