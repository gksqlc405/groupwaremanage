package com.chb.groupwareManage.model.login;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class PasswordUpdateRequest {

    @NotNull
    @Length(min = 5, max = 20)
    @ApiModelProperty(notes = "비밀번호", required = true)
    private String password;


    @NotNull
    @Length(min = 5, max = 20)
    @ApiModelProperty(notes = "새 비밀번호", required = true)
    private String newPassword;

    @NotNull
    @Length(min = 5, max = 20)
    @ApiModelProperty(notes = "새 비밀번호 확인", required = true)
    private String checkPassword;

}
