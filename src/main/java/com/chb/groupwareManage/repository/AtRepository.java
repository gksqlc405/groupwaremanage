package com.chb.groupwareManage.repository;

import com.chb.groupwareManage.entity.AttendanceManage;
import com.chb.groupwareManage.entity.MemberManage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface AtRepository extends JpaRepository<AttendanceManage, Long> {

    Optional<AttendanceManage> findByDateBaseAndMemberManage_Id( LocalDate dateBase, long memberId);

    List<AttendanceManage> findAllByDateBaseGreaterThanEqualAndDateBaseLessThanEqualAndMemberManage_IdOrderByIdDesc(LocalDate dateStart, LocalDate dateEnd, long id);

    long countByMemberManageAndDateBaseGreaterThanEqualAndDateBaseLessThanEqual(MemberManage memberManage, LocalDate dateStart, LocalDate dateEnd);

    List<AttendanceManage> findAllByDateBase(LocalDate dateBase);

}
