package com.chb.groupwareManage.repository;

import com.chb.groupwareManage.entity.MemberManage;
import com.chb.groupwareManage.enums.Department;
import com.chb.groupwareManage.enums.Position;
import com.chb.groupwareManage.model.manager.MemberItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<MemberManage, Long> {

    List<MemberManage> findAllByPositionAndIsEnabledTrueOrderByIdDesc(Position position);
    List<MemberManage> findAllByDepartmentAndIsEnabledTrueOrderByIdDesc(Department department);

    List<MemberManage> findAllByNameOrderByIdDesc(String name);

    Optional<MemberManage> findAllByNameOrderByDateInDesc(String name);

    List<MemberManage> findAllByIsEnabledOrderByIdDesc(Boolean isEnable);

    Optional<MemberManage> findByUsernameAndIsAdmin(String userName, Boolean isAdmin);

    Optional<MemberManage> findByIdAndPassword(long id, String password );

    Optional<MemberManage> findByUsername (String userName);




}
