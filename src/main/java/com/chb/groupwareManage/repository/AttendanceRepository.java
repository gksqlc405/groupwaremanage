package com.chb.groupwareManage.repository;

import com.chb.groupwareManage.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    //특정회원이 출근했나 안했나 확인하기위함
    Optional<Attendance> findByDateWorkAndMemberId(LocalDate dateWork, long memberId);

    List<Attendance> findAllByDateWorkGreaterThanEqualAndDateWorkLessThanEqualAndMemberIdOrderByDateWorkAsc(LocalDate dateStart, LocalDate dateEnd, long memberId);


    long countByMemberIdAndDateWorkGreaterThanEqualAndDateWorkLessThanEqual(long memberId, LocalDate dateStart, LocalDate dateEnd);
}
