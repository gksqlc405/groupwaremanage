package com.chb.groupwareManage.repository;

import com.chb.groupwareManage.entity.Notice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeRepository extends JpaRepository<Notice, Long> {
}
