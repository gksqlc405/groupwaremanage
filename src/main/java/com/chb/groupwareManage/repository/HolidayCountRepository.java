package com.chb.groupwareManage.repository;

import com.chb.groupwareManage.entity.HolidayCount;
import com.chb.groupwareManage.entity.MemberManage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface HolidayCountRepository extends JpaRepository<HolidayCount, Long> {

     HolidayCount findByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndMemberId(LocalDate dateStart, LocalDate dateEnd, long memberId);


     Optional<HolidayCount> findByDateHolidayCountStartLessThanEqualAndDateHolidayCountEndGreaterThanEqualAndMemberId(LocalDate dateCriteria1, LocalDate dateCriteria, long memberId);

}
