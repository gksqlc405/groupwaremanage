package com.chb.groupwareManage.repository;

import com.chb.groupwareManage.entity.BannerUrl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BannerUrlRepository extends JpaRepository<BannerUrl, Long> {
}
