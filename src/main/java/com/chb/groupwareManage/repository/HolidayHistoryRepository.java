package com.chb.groupwareManage.repository;

import com.chb.groupwareManage.entity.HolidayHistory;
import com.chb.groupwareManage.enums.ApprovalStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface HolidayHistoryRepository extends JpaRepository<HolidayHistory, Long> {



    List<HolidayHistory> findAllByIdAndApprovalStatus(long id, ApprovalStatus approvalStatus);

    List<HolidayHistory> findAllByDateCreateGreaterThanEqualAndDateCreateLessThanEqualAndMemberManage_Id(LocalDate dateStart, LocalDate dateEnd, long memberId);



}
