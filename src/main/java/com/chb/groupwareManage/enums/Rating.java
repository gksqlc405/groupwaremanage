package com.chb.groupwareManage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Rating {

    MANAGER("관리자"),
    STAFF("직원");

    private final String name;
}
