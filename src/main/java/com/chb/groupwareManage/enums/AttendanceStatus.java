package com.chb.groupwareManage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AttendanceStatus {

    ATTENDANCE("출근"),
    LEAVE_WORK("퇴근"),
    LEAVE_EARLY("조퇴"),
    NONE("상태없음");



    private final String name;

}
