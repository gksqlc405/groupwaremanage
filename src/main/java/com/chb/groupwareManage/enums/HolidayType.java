package com.chb.groupwareManage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HolidayType {


    ANNUAL_LEAVE("연차"),
    HALF_PM_LEAVE("오후반차"),
    HALF_AM_LEAVE("오전반차"),
    SICK_LEAVE("병가");




    private final String name;
}
