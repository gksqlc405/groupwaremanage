package com.chb.groupwareManage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다.")
    , SUCCESS_ATTENDANCE(0, "정상적으로 출근 하였습니다.")
    , WITHDRAWAL(0, "탈퇴가 성공적으로 이루어젔습니다.")
    , PASSWORD(0, "비밀번호를 변경하였습니다.")
    , LOGIN(0, "로그인 하였습니다" )
    , FAILED(-1, "실패하였습니다.")


    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")
    , NO_EMPLOYEE(-20000, "사원 정보를 찾을수 없습니다.")
    , NO_ATTENDANCE(-30000, "이미 출근 처리 되었습니다.")
    , NO_LEAVE_WORK(-40000, "이미 퇴근 처리 되었습니다.")
    , NO_USER_NAME(-50000, "존재하지 않는 아이디입니다.")
    , NO_PASSWORD(-60000, "비밀번호가 틀렸습니다.")
    , NO_IS_ENABLED(-70000, "퇴사한 회원입니다 관리자에게 문의하세요.")
    , NO_CHECK_PASSWORD(-80000, "비밀번호가 일치하지 않습니다.")
    , ALREADY_ATTENDANCE(-90000, "이미 출근 하였습니다")
    , NO_STATUS(-10000, "출근 기록이 없습니다.")
    , NO_SAME_STATUS(20000, "같은 근로상태로는 다시 변경할수 없습니다.")
    , SAME_USERNAME(-30000, "같은 아이디가 이미 존재합니다.")
    , APPROVED(4000, "이미 승인완료 하였습니다.")
    , COMPANION(-10000, "이미 반려처리 하였습니다.")
    , REMAINING(-20000, "휴가 잔여갯수가 부족합니다.")
    , NO_HOLIDAY_REQUEST(-30000, "휴가 신청 내역이 없습니다." )
    ;

    private final Integer code;
    private final String msg;
}
