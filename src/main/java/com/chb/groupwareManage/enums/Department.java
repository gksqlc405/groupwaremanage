package com.chb.groupwareManage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Department {


    PRODUCTION("생산부"),
    ACCOUNTING("경리부"),
    GENERAL("총무부"),
    SALES("영업부"),
    FINANCE("재무부"),
    DEV("개발부"),
    QUALITY("품질관리부");



    private final String name;
}
