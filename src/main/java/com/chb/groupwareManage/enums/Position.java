package com.chb.groupwareManage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Position {

    BOSS("사장"),
    VICE_PRESIDENT("부사장"),
    DEPARTMENT_HEAD("부장"),
    DEPUTY_DEPARTMENT_HEAD("차장"),
    TEAM_LEADER("팀장"),
    SECTION_CHIEF("과장"),
    ASSISTANT_MANAGER("대리"),
    EMPLOYEE("사원");



    private final String name;
}
