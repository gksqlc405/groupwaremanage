package com.chb.groupwareManage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApprovalStatus {


    REQUESTING("요청중"),
    APPROVED("승인완료"),
    COMPANION("반려");



    private final String name;
}
