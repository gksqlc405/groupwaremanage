package com.chb.groupwareManage.exception;

public class CSameUsernameException extends RuntimeException {
    public CSameUsernameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CSameUsernameException(String msg) {
        super(msg);
    }

    public CSameUsernameException() {
        super();
    }

}
