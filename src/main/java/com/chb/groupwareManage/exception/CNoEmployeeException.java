package com.chb.groupwareManage.exception;

public class CNoEmployeeException extends RuntimeException {
    public CNoEmployeeException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoEmployeeException(String msg) {
        super(msg);
    }

    public CNoEmployeeException() {
        super();
    }
}
