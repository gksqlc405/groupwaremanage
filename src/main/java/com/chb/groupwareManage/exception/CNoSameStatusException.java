package com.chb.groupwareManage.exception;

public class CNoSameStatusException extends RuntimeException {
    public CNoSameStatusException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoSameStatusException(String msg) {
        super(msg);
    }

    public CNoSameStatusException() {
        super();
    }
}
