package com.chb.groupwareManage.exception;

public class CApprovedException extends RuntimeException {
    public CApprovedException(String msg, Throwable t) {
        super(msg, t);
    }

    public CApprovedException(String msg) {
        super(msg);
    }

    public CApprovedException() {
        super();
    }
}
