package com.chb.groupwareManage.exception;

public class CAlreadyAttendanceException  extends RuntimeException {
    public CAlreadyAttendanceException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyAttendanceException(String msg) {
        super(msg);
    }

    public CAlreadyAttendanceException() {
        super();
    }
}
