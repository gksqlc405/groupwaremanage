package com.chb.groupwareManage.exception;

public class CNoRemainingException extends RuntimeException {
    public CNoRemainingException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoRemainingException(String msg) {
        super(msg);
    }

    public CNoRemainingException() {
        super();
    }
}
