package com.chb.groupwareManage.exception;

public class CNoPasswordException extends RuntimeException {
    public CNoPasswordException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoPasswordException(String msg) {
        super(msg);
    }

    public CNoPasswordException() {
        super();
    }
}
