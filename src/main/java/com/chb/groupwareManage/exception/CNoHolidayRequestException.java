package com.chb.groupwareManage.exception;

public class CNoHolidayRequestException extends RuntimeException {
    public CNoHolidayRequestException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoHolidayRequestException(String msg) {
        super(msg);
    }

    public CNoHolidayRequestException() {
        super();
    }
}
