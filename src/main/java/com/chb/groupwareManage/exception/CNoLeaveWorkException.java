package com.chb.groupwareManage.exception;

public class CNoLeaveWorkException extends RuntimeException {
    public CNoLeaveWorkException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoLeaveWorkException(String msg) {
        super(msg);
    }

    public CNoLeaveWorkException() {
        super();
    }

}

