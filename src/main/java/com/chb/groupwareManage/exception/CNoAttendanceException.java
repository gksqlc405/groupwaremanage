package com.chb.groupwareManage.exception;

public class CNoAttendanceException extends RuntimeException {
    public CNoAttendanceException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoAttendanceException(String msg) {
        super(msg);
    }

    public CNoAttendanceException() {
        super();
    }
}

