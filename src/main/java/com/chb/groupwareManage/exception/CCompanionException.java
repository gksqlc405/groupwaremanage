package com.chb.groupwareManage.exception;

public class CCompanionException  extends RuntimeException {
    public CCompanionException(String msg, Throwable t) {
        super(msg, t);
    }

    public CCompanionException(String msg) {
        super(msg);
    }

    public CCompanionException() {
        super();
    }
}
