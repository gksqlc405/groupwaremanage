package com.chb.groupwareManage.exception;

public class CNoCheckPasswordException extends RuntimeException {
    public CNoCheckPasswordException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoCheckPasswordException(String msg) {
        super(msg);
    }

    public CNoCheckPasswordException() {
        super();
    }
}
