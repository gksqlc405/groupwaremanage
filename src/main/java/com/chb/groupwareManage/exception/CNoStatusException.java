package com.chb.groupwareManage.exception;

public class CNoStatusException extends RuntimeException {
    public CNoStatusException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoStatusException(String msg) {
        super(msg);
    }

    public CNoStatusException() {
        super();
    }
}
