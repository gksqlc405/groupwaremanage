package com.chb.groupwareManage.exception;

public class CNoIsEnabledException extends RuntimeException {
    public CNoIsEnabledException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoIsEnabledException(String msg) {
        super(msg);
    }

    public CNoIsEnabledException() {
        super();
    }

}
